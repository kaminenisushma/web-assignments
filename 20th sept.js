//squaring
const square = arr => {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    result.push(arr[i] * arr[i]);
  }
  return result;
};
console.log(square([1, 2, 3, 4]));


//even numbers of an array

const even = arr => {
  let result = [];
  for (let i = 0; i < arr.length; i++) {
    result.push(arr[i] % 2 == 0);
  }
  return result;
};
console.log(even([1, 2, 3, 4]));

//sum of an array

const sum = arr => {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum = sum + arr[i];
  }
  return sum;
};
console.log(sum([1, 2, 3, 4]));

//max of an array
const max = arr => {
  let max = 0;
  for (let i = 0; i < arr.length; i++) {
    arr[i] > max;
    max = arr[i];
  }
  return max;
};
console.log(max([1, 2, 3, 4]));


//index of value in an array

const index = (arr, val) => {
  for (let i = 0; i <= arr.length; i++) {
    if (arr[i] == val) {
      return arr[i];
    }
  }
};



