//map
const even = n => n % 2 == 0;
const map = (f, arr) => {
  const result = [];
  for (const e of arr) result.push(f(e));
  return result;
};
console.log(map(even, [1, 2, 3, 4, 5]));


//filter
const even = n => n % 2 == 0;
const filter = (f, arr) => {
  const result = [];
  for (const e of arr) if (f(e)) result.push(e);

  return result;
};
console.log(filter(even, [1, 2, 3, 4, 5]));


//reduce
const add = (x, y) => x + y;

const reduce = (f, init, arr) => {
  let result = init;
  for (const e of arr) result = f(result, e);

  return result;
};
console.log(reduce(add, 3, [1, 2, 3]));


//reverse
const reverse = arr => {
  let result = [];
  for (let i = arr.length - 1; i >= 0; i--) {
    result.push(arr[i]);
  }
  return result;
};
console.log(reverse([1, 2, 3, 4]));


//takewhile
const even = n => n % 2 ==0;
const take_while = (f, arr) => {
  const result = [];
  for (const e of arr)
    if (f(e)) result.push(e);
    else return result;
};
console.log(take_while(even, [3, 4, 5]));


//drop while
const even = n => n % 2 == 0;
const drop_while = (f, arr) => {
  for (let i = 0; i < arr.length; i++)
    if (even(arr[i]) == true) {
      if (even(arr[i + 1]) == false) return arr[i + 1];
      return arr;
    }
};
console.log(drop_while(even, [2, 3, 4, 5]));

