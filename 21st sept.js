//concatination
const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
const arr3 = arr1.concat(arr2);
console.log(arr3);


//prime

const prime = n => {
  if (n >= 2) {
    for (let i = 2; i < n; i++) {
      if (n % i == 0) return false;
    }
    return true;
  }
  return false;
};
console.log(prime(6));


//perfect
var num = 28;
var factors = [];
for (var i = 1; i <= num - 1; i++) {
  if (num % i == 0) {
    factors.push(i);
  }
}
var sum = factors.reduce(function(a, b) {
  return a + b;
});
if (sum == num && (sum + num) / 2 == num) {
  console.log("Perfect number");
} else console.log(num + " is not a Perfect Number");

//take
const take = (n, arr) => {
  const result = [];
  for (let i = 0; i < n; i++) result.push(arr[i]);
  return result;
};

console.log(take(4, [1, 2, 3, 4, 5, 6, 7, 8]));


//drop
const drop = (n, arr) => {
  const result = [];
  for (let i = n; i < arr.length; i++) result.push(arr[i]);
  return result;
};

console.log(drop(4, [1, 2, 3, 4, 5, 6, 7, 8]));


//reverse
const reverse = arr => {
  let result = [];
  for (let i = arr.length - 1; i >= 0; i--) {
    result.push(arr[i]);
  }
  return result;
};
console.log(reverse([1, 2, 3, 4]));

