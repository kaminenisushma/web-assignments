//merge
const merge = (obj, obj2) => {
  const result = {};
  for (let k in obj) result[k] = obj[k];
  for (let k in obj2) result[k] = obj2[k];
  return result;
};
const rectangle = merge({ x: 1, y: 2 }, { width: 10, height: 20 });
console.log(rectangle);
